package org.eclipse.tracecompass.incubator.internal.validator.core.analysis;

public interface IEventConstants {
    enum Acceptance {
        VIOLATED,
        NON_VIOLATED
    }

    enum ActivatedState {
        ACTIVATED,
        DEACTIVATED
    }
}
