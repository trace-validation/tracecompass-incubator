package org.eclipse.tracecompass.incubator.internal.validator.core.analysis;

import org.eclipse.tracecompass.incubator.internal.validator.core.analysis.IEventConstants.Acceptance;
import org.eclipse.tracecompass.incubator.internal.validator.core.analysis.IEventConstants.ActivatedState;
import org.eclipse.tracecompass.statesystem.core.ITmfStateSystemBuilder;
import org.eclipse.tracecompass.statesystem.core.statevalue.ITmfStateValue;
import org.eclipse.tracecompass.statesystem.core.statevalue.TmfStateValue;
import org.eclipse.tracecompass.tmf.core.event.ITmfEvent;
import org.eclipse.tracecompass.tmf.core.statesystem.AbstractTmfStateProvider;
import org.eclipse.tracecompass.tmf.core.statesystem.ITmfStateProvider;
import org.eclipse.tracecompass.tmf.core.trace.ITmfTrace;

import trace_validation.nba.Nba;
import trace_validation.nba.State;
import trace_validation.trace_validator.reconstructed_state.Proposition;
import trace_validation.trace_validator.reconstructed_state.ReconstructedState;
import trace_validation.trace_validator.reconstructed_state.RequirementAcceptedException;
import trace_validation.trace_validator.reconstructed_state.RequirementViolatedException;
import trace_validation.trace_validator.simulated_automaton.SimulatedAutomaton;
import trace_validation.trace_validator.simulated_automaton.SimulatedState;

public class ReqVerificationStateProvider extends AbstractTmfStateProvider {

    private Nba requirement;

    private SimulatedAutomaton automaton;

    private ReconstructedState reconstructedState;

    private IEventConstants.Acceptance acceptance = Acceptance.VIOLATED;

    public ReqVerificationStateProvider(ITmfTrace trace, Nba requirement) {
        super(trace, "org.eclipse.tracecompass.processing.time.state.provider");

        this.requirement = requirement;
        automaton = new SimulatedAutomaton(requirement);
        reconstructedState = new ReconstructedState(requirement.getWhere());
    }

    @Override
    public int getVersion() {
        return 0;
    }

    @Override
    public ITmfStateProvider getNewInstance() {
        return new ReqVerificationStateProvider(this.getTrace(), requirement);
    }

    @Override
    protected void eventHandle(ITmfEvent event) {
        // Update state
        reconstructedState.update(event);

        try {
            // Update automaton
            automaton.execute(reconstructedState);

            // Check if the requirement is now violated
            automaton.validate();
        } catch (RequirementAcceptedException acc) {
            this.acceptance = Acceptance.NON_VIOLATED;
        } catch (RequirementViolatedException rej) {
            this.acceptance = Acceptance.VIOLATED;
        }


        // Update State System
        final ITmfStateSystemBuilder stateSystem = getStateSystemBuilder();
        if (stateSystem == null){
            return;
        }

        /**
        * Attribute tree:
        * --------------
        * <Requirement name>
        *     |
        *     |-Acceptance     -> IEventConstants.Acceptance
        *     |-States
        *     |   |
        *     |   |-<State ID> -> IEventConstants.ActivatedState
        *     |
        *     |-APs
        *         |
        *         |-<AP>       -> boolean
        */

        // Acceptance condition
        int acceptQuark = stateSystem.getQuarkAbsoluteAndAdd(requirement.getName(), "Acceptance");
        // Get new value
        ITmfStateValue stateValue = TmfStateValue.newValueString(this.acceptance.toString());
        // Get time of event
        long t = event.getTimestamp().getValue();
        // Update
        stateSystem.modifyAttribute(t, stateValue, acceptQuark);

        // Automaton states
        for (State state : requirement.getAutomaton().getStates()) {
            int stateQuark = stateSystem.getQuarkAbsoluteAndAdd(requirement.getName(), "States", state.getID());
            // Get new value
            ActivatedState isActive = ActivatedState.DEACTIVATED;
            for (SimulatedState currentState : automaton.getCurrentStates()) {
                if (state.getID().contentEquals(currentState.getId())) {
                    isActive = ActivatedState.ACTIVATED;
                    break;
                }
            }
            stateValue = TmfStateValue.newValueString(isActive.toString());
            // Get time of event
            t = event.getTimestamp().getValue();
            // Update
            stateSystem.modifyAttribute(t, stateValue, stateQuark);
        }

        // AP states
        for (Proposition prop : reconstructedState.getState()) {
            int stateQuark = stateSystem.getQuarkAbsoluteAndAdd(requirement.getName(), "APs", prop.getId());
            // Get new value
            stateValue = TmfStateValue.newValueInt(prop.isActive() ? 1 : 0);
            // Get time of event
            t = event.getTimestamp().getValue();
            // Update
            stateSystem.modifyAttribute(t, stateValue, stateQuark);
        }

    }
}
