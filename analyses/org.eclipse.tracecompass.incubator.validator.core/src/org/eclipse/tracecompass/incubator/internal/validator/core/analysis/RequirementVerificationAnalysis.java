package org.eclipse.tracecompass.incubator.internal.validator.core.analysis;

import trace_validation.nba.Nba;
import trace_validation.nba.Where;
import trace_validation.requirement_converter.interfaces.ltl.Operator;
import trace_validation.requirement_converter.interfaces.ltl.Requirement;
import trace_validation.requirement_converter.interfaces.ltl.RequirementParser;
import trace_validation.requirement_converter.ltl2nba.*;

import java.io.*;
import java.util.*;
import java.util.Set;

import com.google.common.collect.ImmutableSet;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.tracecompass.tmf.core.analysis.requirements.TmfAbstractAnalysisRequirement;
import org.eclipse.tracecompass.tmf.core.analysis.requirements.TmfAbstractAnalysisRequirement.PriorityLevel;
import org.eclipse.tracecompass.tmf.core.analysis.requirements.TmfAnalysisEventRequirement;
import org.eclipse.tracecompass.tmf.core.statesystem.ITmfStateProvider;
import org.eclipse.tracecompass.tmf.core.statesystem.TmfStateSystemAnalysisModule;
import org.eclipse.tracecompass.tmf.core.trace.ITmfTrace;

public class RequirementVerificationAnalysis extends TmfStateSystemAnalysisModule {

    private Nba automaton = null;

    /** The analysis's requirements. Only set after the trace is set. */
    private Set<TmfAbstractAnalysisRequirement> fAnalysisRequirements;

    public RequirementVerificationAnalysis() {
        // TODO Auto-generated constructor stub
    }

    @Override
    protected void canceling() {
        // TODO Auto-generated method stub

    }

    @Override
    public @NonNull Iterable<@NonNull TmfAbstractAnalysisRequirement> getAnalysisRequirements() {

        // Parse requirement
        System.out.println("CURRENT DIR: " + System.getProperty("user.dir"));
        createAutomaton("");

        // Check if some events in the current trace are useful
        Set<String> usefulEvents = new HashSet<>();
        for (Where where : automaton.getWhere()) {
            // Add events, since it's a set doubles are remove automatically
            usefulEvents.add(where.getOn());
            usefulEvents.add(where.getOff());
        }

        /* Initialize the requirements for the analysis: events */
        Set<TmfAbstractAnalysisRequirement> requirements = fAnalysisRequirements;
        TmfAbstractAnalysisRequirement eventsReq = new TmfAnalysisEventRequirement(usefulEvents, PriorityLevel.AT_LEAST_ONE);
        requirements = ImmutableSet.of(eventsReq);
        fAnalysisRequirements = requirements;

        return requirements;
    }

    @Override
    protected @NonNull ITmfStateProvider createStateProvider() {
        /** jorrit: happy printing showing that the analysis has been execute.
         *
         *  Maybe here you can also:
         *  - initialize/execute the LTL -> NBA translator?
         *  - pass to the state provider the NBA data structure?
         */

        System.out.println("Creating requirement verification analysis state provider.");

        ITmfTrace trace = getTrace();
        if (trace == null) {
            throw new IllegalStateException();
        }
        return new ReqVerificationStateProvider(trace, automaton);
    }

    private void createAutomaton(String path) {
        Requirement req = null;
        try {
            String json = readFile(path);
            req = RequirementParser.fromJsonString(json);
        } catch (IOException e) {
            e.printStackTrace();
        }

        LtlSimplifier simplifier = new LtlSimplifier();
        Operator simplifiedFormula = simplifier.simplify(req.getLTL().getFormalLTL()[0]);

        TableauCreator tc = new TableauCreator();
        Set<TableauNode> tableauNodes = tc.createGraph(simplifiedFormula);

        // Tableau -> LGBA -> GBA -> NBA
        NBA internalNBA = new NBA(new GBA(new LGBA(tableauNodes, simplifiedFormula)));

        this.automaton = internalNBA.toNBAInterface(req);
    }

    private static String readFile(String file) throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader (file));
        String         line = null;
        StringBuilder  stringBuilder = new StringBuilder();
        String         ls = System.getProperty("line.separator");

        try {
            while((line = reader.readLine()) != null) {
                stringBuilder.append(line);
                stringBuilder.append(ls);
            }

            return stringBuilder.toString();
        } finally {
            reader.close();
        }
    }

}
